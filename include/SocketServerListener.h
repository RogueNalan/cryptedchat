#ifndef SOCKETSERVERLISTENER_H
#define SOCKETSERVERLISTENER_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "Socket.h"
#include "SocketListener.h"


class SocketServerListener
{
public:
    SocketServerListener();
    virtual ~SocketServerListener();
    virtual void OnSocketConnected(Socket *socket) = 0;
};

#endif // SOCKETSERVERLISTENER_H
