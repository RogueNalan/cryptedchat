#ifndef SOCKETSERVER_H
#define SOCKETSERVER_H

#include <iostream>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/socket.h>

#include "SocketServerListener.h"


class SocketServer : public wxEvtHandler
{
public:
    SocketServer(SocketServerListener *listener);
    virtual ~SocketServer();
    void OnSocketServerEvent(wxSocketEvent &event);

protected:
    SocketServerListener *m_listener;
    wxSharedPtr<wxSocketServer> m_socket_server;
    enum
    {
        idSocketServer
    };

    DECLARE_EVENT_TABLE()
};

#endif // SOCKETSERVER_H
