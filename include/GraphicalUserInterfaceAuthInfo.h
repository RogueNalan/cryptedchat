#ifndef GRAPHICALUSERINTERFACEAUTHINFO_H
#define GRAPHICALUSERINTERFACEAUTHINFO_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/filename.h>


class GraphicalUserInterfaceAuthInfo : public wxDialog
{
public:
    GraphicalUserInterfaceAuthInfo(wxWindow *parent, wxSharedPtr<wxString> config_path);
    virtual ~GraphicalUserInterfaceAuthInfo();
    wxSharedPtr<wxString> GetConfigPath();
    wxSharedPtr<wxString> GetName();

protected:
    enum
    {
        idButtonChooseConfig,
        idTextCtrlName
    };
    wxButton *m_button_choose_config;
    wxSharedPtr<wxString> m_config_path;
    wxStaticText *m_static_text_config_path;
    wxTextCtrl *m_text_ctrl_name;

protected:
    bool HasConfigPath();
    bool HasName();
    void ShowConfigPath();

private:
    void OnButtonClose(wxCommandEvent &event);
    void OnButtonOK(wxCommandEvent &event);
    void OnChooseConfig(wxCommandEvent &event);
    void OnClose(wxCloseEvent &event);
    DECLARE_EVENT_TABLE();
};

#endif // GRAPHICALUSERINTERFACEAUTHINFO_H
