#ifndef GRAPHICALUSERINTERFACE_H
#define GRAPHICALUSERINTERFACE_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "UserInterface.h"
#include "GraphicalUserInterfaceMain.h"


class GraphicalUserInterface : public UserInterface
{
public:
    explicit GraphicalUserInterface(UserInterfaceListener *listener);
    virtual ~GraphicalUserInterface();
    virtual void Close();
    virtual void Show();
    virtual void ShowError(wxSharedPtr<wxString> error);
    virtual void ShowMessage(wxSharedPtr<wxString> name, wxSharedPtr<wxString> message);

protected:
    GraphicalUserInterfaceMain *m_main;
};

#endif // GRAPHICALUSERINTERFACE_H
