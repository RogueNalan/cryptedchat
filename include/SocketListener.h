#ifndef SOCKETLISTENER_H
#define SOCKETLISTENER_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif


class Socket;

class SocketListener
{
public:
    SocketListener();
    virtual ~SocketListener();
    virtual void OnMessage(wxSharedPtr<wxString> message) = 0;
    virtual void OnSocketLost(Socket *socket) = 0;
};

#endif // SOCKETLISTENER_H
