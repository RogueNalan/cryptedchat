#ifndef SERVER_H
#define SERVER_H

#include <set>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "Config.h"
#include "Socket.h"
#include "SocketListener.h"
#include "SocketServer.h"
#include "SocketServerListener.h"


class Server : public SocketServerListener, public SocketListener
{
public:
    static Server *GetInstance(wxSharedPtr<Config>);
    void OnMessage(wxSharedPtr<wxString> message);
    void OnSocketConnected(Socket *socket);
    void OnSocketLost(Socket *socket);

protected:
    wxSharedPtr<SocketServer> m_socket_server;
    wxSharedPtr< std::set<Socket*> > m_sockets;

private:
    static Server *m_instance;

private:
    Server(wxSharedPtr<Config> config);
    virtual ~Server();
};

#endif // SERVER_H
