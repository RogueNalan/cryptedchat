#ifndef XORCYPHER_H
#define XORCYPHER_H

#include "Cypher.h"


class XORCypher : public Cypher
{
public:
    XORCypher(char key);
    ~XORCypher();
    virtual wxSharedPtr<char> Encrypt(wxSharedPtr<char>, int);
    virtual wxSharedPtr<char> Decrypt(wxSharedPtr<char>, int);

private:
    wxSharedPtr<char> Convert(wxSharedPtr<char> message, int length);

    char key;
};

#endif // XORCYPHER_H
