#ifndef USERINTERFACELISTENER_H
#define USERINTERFACELISTENER_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif


class UserInterface;


class UserInterfaceListener
{
public:
    UserInterfaceListener();
    virtual ~UserInterfaceListener();
    virtual void OnExit() = 0;
    virtual void OnAuthInfoChange(wxSharedPtr<wxString> name, wxSharedPtr<wxString> config_path) = 0;
    virtual void OnSend(wxSharedPtr<wxString> message) = 0;

protected:
    wxSharedPtr<UserInterface> m_user_interface;
};

#include "UserInterface.h"

#endif // USERINTERFACELISTENER_H
