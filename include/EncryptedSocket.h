#ifndef ENCRYPTEDSOCKET_H
#define ENCRYPTEDSOCKET_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/socket.h>

#include "Cypher.h"
#include "Socket.h"
#include "SocketListener.h"


class EncryptedSocket : public Socket, public SocketListener
{
public:
    EncryptedSocket(wxSharedPtr<wxSocketBase> socket, wxSharedPtr<Cypher> cypher);
    EncryptedSocket(wxSharedPtr<wxSocketBase> socket, SocketListener *listener, wxSharedPtr<Cypher> cypher);
    virtual ~EncryptedSocket();
    virtual void OnMessage(wxSharedPtr<wxString> message);
    virtual void OnSocketLost(Socket *socket);
    virtual void Send(wxSharedPtr<wxString> message);
    virtual void SetListener(SocketListener *listener);

protected:
    wxSharedPtr<Cypher> m_cypher;
    SocketListener *m_encrypted_socket_listener;
};

#endif // ENCRYPTEDSOCKET_H
