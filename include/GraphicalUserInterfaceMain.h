#ifndef GRAPHICALUSERINTERFACEMAIN_H
#define GRAPHICALUSERINTERFACEMAIN_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "UserInterfaceListener.h"
#include "GraphicalUserInterfaceAuthInfo.h"


class GraphicalUserInterfaceMain : public wxDialog
{
public:
    explicit GraphicalUserInterfaceMain(UserInterfaceListener *listener);
    virtual ~GraphicalUserInterfaceMain();
    void AddStringToMessages(wxSharedPtr<wxString> str);
    void Show();

protected:
    enum
    {
        idTextCtrlMessage,
        idButtonSend
    };
    wxListBox *m_list_box_messages;
    wxTextCtrl *m_text_ctrl_message;
    wxButton *m_button_send;
    UserInterfaceListener *m_listener;

protected:
    void Send();
    void OpenAuthInfo();

private:
    void OnClose(wxCloseEvent &event);
    void OnMessageEnter(wxCommandEvent &event);
    void OnSend(wxCommandEvent &event);
    DECLARE_EVENT_TABLE()
};

#endif // GRAPHICALUSERINTERFACEMAIN_H
