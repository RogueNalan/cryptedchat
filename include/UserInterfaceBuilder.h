#ifndef USERINTERFACEBUILDER_H
#define USERINTERFACEBUILDER_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

class UserInterface;
class UserInterfaceListener;


class UserInterfaceBuilder
{
public:
    UserInterfaceBuilder();
    virtual ~UserInterfaceBuilder();
    wxSharedPtr<UserInterface> GetUserInterface(UserInterfaceListener *user_interface_listener);
};

#endif // USERINTERFACEBUILDER_H
