#ifndef CYPHER_H
#define CYPHER_H


#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif


class Cypher
{
public:
    Cypher();
    virtual ~Cypher();
    virtual wxSharedPtr<char> Encrypt(wxSharedPtr<char> message, int length) = 0;
    virtual wxSharedPtr<char> Decrypt(wxSharedPtr<char> message, int length) = 0;
};

#endif // CYPHER_H
