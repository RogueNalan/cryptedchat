#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "UserInterfaceListener.h"


class UserInterface
{
public:
    explicit UserInterface(UserInterfaceListener *listener);
    virtual ~UserInterface();
    virtual void Close() = 0;
    virtual void Show() = 0;
    virtual void ShowError(wxSharedPtr<wxString> error) = 0;
    virtual void ShowMessage(wxSharedPtr<wxString> name, wxSharedPtr<wxString> message) = 0;

protected:
    UserInterfaceListener* m_listener;
};

#endif // USERINTERFACE_H
