#ifndef SOCKET_H
#define SOCKET_H

#include <stdint.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/socket.h>
#include <wx/file.h>

#include "SocketListener.h"

enum msg_t {MsgText, MsgFile};

class Socket : public wxEvtHandler
{
public:
    Socket(wxSharedPtr<wxSocketBase> socket);
    Socket(wxSharedPtr<wxSocketBase> socket, SocketListener *listener);
    virtual ~Socket();
    void OnSocketEvent(wxSocketEvent &event);
    void Send(wxSharedPtr<wxString> message);
    void SendFile(wxSharedPtr<wxString> filedest);
    void SetListener(SocketListener *listener);

protected:
    wxSharedPtr<wxSocketBase> m_socket;
    SocketListener *m_listener;
    enum
    {
        idSocket
    };

protected:
    void Init(wxSharedPtr<wxSocketBase> socket);

    DECLARE_EVENT_TABLE()
};

#endif // SOCKET_H
