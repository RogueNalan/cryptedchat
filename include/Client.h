#ifndef CLIENT_H
#define CLIENT_H

#include <stdint.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "Socket.h"
#include "SocketFabric.h"
#include "UserInterface.h"


class Client : public UserInterfaceListener, public SocketListener
{
public:
    static Client *GetInstance();
    void OnAuthInfoChange(wxSharedPtr<wxString> name, wxSharedPtr<wxString> config_path);
    void OnExit();
    void OnMessage(wxSharedPtr<wxString> message);
    void OnSend(wxSharedPtr<wxString> message);
    void OnSocketLost(Socket *socket);

protected:
    wxSharedPtr<wxString> m_name;
    wxSharedPtr<Socket> m_socket;

protected:
    void Send(wxSharedPtr<wxString> message);

private:
    static Client *m_instance;

private:
    explicit Client();
    virtual ~Client();
};

#endif // CLIENT_H
