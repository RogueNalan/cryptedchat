#ifndef CONFIG_H
#define CONFIG_H

#include <utility>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/file.h>
#include <wx/socket.h>


class Config
{
public:
    Config(wxSharedPtr<wxIPV4address> address);
    Config(wxSharedPtr<wxString> path);
    virtual ~Config();
    wxSharedPtr<wxIPV4address> GetAddress();
    wxSharedPtr<char> GetKey1();
    int GetKey1Length();
    wxSharedPtr<char> GetKey2();
    int GetKey2Length();
    void Save(wxSharedPtr<wxString> path);

protected:
    static const int kKey1LengthUsual = 541; // The 100th prime is 541
    static const int kKey2LengthUsual = 547; // The 101st prime is 547
    static const int kVersion = 1;
    wxSharedPtr<wxIPV4address> m_address;
    int m_key1_length;
    wxSharedPtr<char> m_key1;
    int m_key2_length;
    wxSharedPtr<char> m_key2;

protected:
    wxSharedPtr<char> GetKey(int length);
    std::pair<int, wxSharedPtr<char> > ReadBuffer(wxSharedPtr<wxFile>);
    int ReadInt(wxSharedPtr<wxFile>);
    wxSharedPtr<wxString> ReadString(wxSharedPtr<wxFile>);
    void SaveBuffer(wxSharedPtr<wxFile>, wxSharedPtr<char> buffer, int length);
    void SaveInt(wxSharedPtr<wxFile>, int number);
    void SaveString(wxSharedPtr<wxFile>, wxSharedPtr<wxString> str);
};

#endif // CONFIG_H
