#ifndef SOCKETFABRIC_H
#define SOCKETFABRIC_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/socket.h>

#include "Cypher.h"
#include "EncryptedSocket.h"


class SocketFabric
{
public:
    static SocketFabric *GetInstance(wxSharedPtr<Cypher> cypher);
    wxSharedPtr<Socket> CreateSocket(wxSharedPtr<wxIPV4address> address, SocketListener *listener);

protected:
    wxSharedPtr<Cypher> m_cypher;

private:
    static SocketFabric *m_instance;

private:
    SocketFabric(wxSharedPtr<Cypher> cypher);
    virtual ~SocketFabric();
};

#endif // SOCKETFABRIC_H
