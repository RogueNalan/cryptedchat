#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "EncryptedChatApp.h"

#include "Config.h"

IMPLEMENT_APP(EncryptedChatApp);

bool EncryptedChatApp::OnCmdLineParsed(wxCmdLineParser &parser)
{
    wxLog* logger = new wxLogStream(&std::cout);
    wxLog::SetActiveTarget(logger);
    wxSharedPtr<wxString> str(new wxString());
    if(parser.Found("s", &*str)) // server
    {
        wxSharedPtr<Config> config(new Config(str));
        Server::GetInstance(config);
    }
    else if(parser.Found("c", &*str)) // config
    {
        wxSharedPtr<wxString> path(new wxString());
        if(parser.Found("p", &*path))
        {
            wxSharedPtr<wxIPV4address> address(new wxIPV4address());
            address->Hostname(*str);
            Config config(address);
            config.Save(path);
            return false;
        }
        else
        {
            wxLogMessage("Specify path using -p option");
            return false;
        }
    }
    else
    {
        Client::GetInstance();
    }
    return true;
}

void EncryptedChatApp::OnInitCmdLine(wxCmdLineParser &parser)
{
    parser.SetDesc(kCmdLineDesc);
    parser.SetSwitchChars("-");
}
