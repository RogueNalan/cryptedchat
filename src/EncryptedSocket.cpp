#include "EncryptedSocket.h"

EncryptedSocket::~EncryptedSocket()
{
    //dtor
}

EncryptedSocket::EncryptedSocket(wxSharedPtr<wxSocketBase> socket, wxSharedPtr<Cypher> cypher) :
    Socket(socket),
    m_cypher(cypher),
    m_encrypted_socket_listener(NULL)
{
    this->SetListener(this);
}

EncryptedSocket::EncryptedSocket(wxSharedPtr<wxSocketBase> socket, SocketListener *listener, wxSharedPtr<Cypher> cypher) :
    Socket(socket),
    m_cypher(cypher),
    m_encrypted_socket_listener(listener)
{
    this->SetListener(this);
}

void EncryptedSocket::OnMessage(wxSharedPtr<wxString> message)
{
    wxLogMessage("Decrypt %s", *message);
    m_encrypted_socket_listener->OnMessage(m_cypher->Decrypt(message));
}

void EncryptedSocket::OnSocketLost(Socket *socket)
{
    m_encrypted_socket_listener->OnSocketLost(socket);
}

void EncryptedSocket::Send(wxSharedPtr<wxString> message)
{
    wxLogMessage("Encrypt %s", *message);
    wxLogMessage(*m_cypher->Encrypt(message));
    Socket::Send(m_cypher->Encrypt(message));
}

void EncryptedSocket::SetListener(SocketListener *listener)
{
    m_encrypted_socket_listener = listener;
}
