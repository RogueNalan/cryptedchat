#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "GraphicalUserInterface.h"

GraphicalUserInterface::GraphicalUserInterface(UserInterfaceListener *listener) :
    UserInterface(listener),
    m_main(new GraphicalUserInterfaceMain(listener))
{
}

GraphicalUserInterface::~GraphicalUserInterface()
{
}

void GraphicalUserInterface::Close()
{
    m_main->Destroy();
}

void GraphicalUserInterface::Show()
{
    m_main->Show();
}

void GraphicalUserInterface::ShowError(wxSharedPtr<wxString> error)
{
    m_main->AddStringToMessages(wxSharedPtr<wxString>(new wxString("ERROR: " + *error)));
}

void GraphicalUserInterface::ShowMessage(wxSharedPtr<wxString> author, wxSharedPtr<wxString> message)
{
    m_main->AddStringToMessages(wxSharedPtr<wxString>(new wxString(*author + ": " + *message)));
}
