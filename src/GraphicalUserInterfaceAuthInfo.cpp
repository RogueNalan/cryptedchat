#include "GraphicalUserInterfaceAuthInfo.h"


BEGIN_EVENT_TABLE(GraphicalUserInterfaceAuthInfo, wxDialog)
    EVT_BUTTON(idButtonChooseConfig, GraphicalUserInterfaceAuthInfo::OnChooseConfig)
    EVT_BUTTON(wxID_CLOSE, GraphicalUserInterfaceAuthInfo::OnButtonClose)
    EVT_BUTTON(wxID_OK, GraphicalUserInterfaceAuthInfo::OnButtonOK)
    EVT_CLOSE(GraphicalUserInterfaceAuthInfo::OnClose)
END_EVENT_TABLE()

GraphicalUserInterfaceAuthInfo::GraphicalUserInterfaceAuthInfo(wxWindow *parent, wxSharedPtr<wxString> config_path) :
    wxDialog(parent, wxID_ANY, "Enter your details", wxDefaultPosition, wxSize(400, 210), wxDEFAULT_DIALOG_STYLE & ~wxRESIZE_BORDER),
    m_config_path(config_path)
{
    const int kBorderWidthLarge = 20;
    const int kBorderWidth = 5;
    wxBoxSizer *dialog_sizer_outer = new wxBoxSizer(wxVERTICAL);
    wxFlexGridSizer *dialog_sizer = new wxFlexGridSizer(4, 1, 0, 0);
    dialog_sizer->AddGrowableCol(0);

    wxBoxSizer *config_sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *static_text_config_label = new wxStaticText(this, wxID_STATIC, "Config:");
    config_sizer->Add(static_text_config_label, 0, wxALIGN_CENTER_VERTICAL, kBorderWidth);
    m_static_text_config_path = new wxStaticText(this, wxID_STATIC, "");
    this->ShowConfigPath();
    config_sizer->Add(m_static_text_config_path, 0, wxLEFT | wxALIGN_CENTER_VERTICAL, kBorderWidth);
    config_sizer->AddStretchSpacer();
    m_button_choose_config = new wxButton(this, idButtonChooseConfig, "Choose");
    config_sizer->Add(m_button_choose_config, 0, 0, kBorderWidth);
    dialog_sizer->Add(config_sizer, 0, wxGROW | wxTOP, kBorderWidthLarge);

    wxBoxSizer *name_sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText *static_text_name = new wxStaticText(this, wxID_STATIC, "Name:");
    name_sizer->Add(static_text_name, 0, wxALIGN_CENTER_VERTICAL, kBorderWidth);
    m_text_ctrl_name = new wxTextCtrl(this, idTextCtrlName, "", wxDefaultPosition);
    name_sizer->Add(m_text_ctrl_name, 1, wxEXPAND | wxLEFT, kBorderWidth);
    dialog_sizer->Add(name_sizer, 0, wxGROW | wxTOP, kBorderWidthLarge);

    dialog_sizer->AddGrowableRow(2);
    dialog_sizer->AddStretchSpacer();

    dialog_sizer->Add(this->CreateStdDialogButtonSizer(wxOK | wxCLOSE), 0, wxGROW, kBorderWidthLarge);

    dialog_sizer_outer->Add(dialog_sizer, 1, wxGROW | wxALIGN_BOTTOM | (wxALL & ~wxTOP), kBorderWidthLarge);
    this->SetSizer(dialog_sizer_outer);
}

GraphicalUserInterfaceAuthInfo::~GraphicalUserInterfaceAuthInfo()
{
    //dtor
}

wxSharedPtr<wxString> GraphicalUserInterfaceAuthInfo::GetConfigPath()
{
    return m_config_path;
}

wxSharedPtr<wxString> GraphicalUserInterfaceAuthInfo::GetName()
{
    return wxSharedPtr<wxString>(new wxString(m_text_ctrl_name->GetValue()));
}

bool GraphicalUserInterfaceAuthInfo::HasConfigPath()
{
    return NULL != m_config_path;
}

bool GraphicalUserInterfaceAuthInfo::HasName()
{
    return m_text_ctrl_name->GetValue() != "";
}

void GraphicalUserInterfaceAuthInfo::OnButtonClose(wxCommandEvent &event)
{
    this->EndModal(wxID_CLOSE);
}

void GraphicalUserInterfaceAuthInfo::OnButtonOK(wxCommandEvent &event)
{
    if(!this->HasConfigPath())
    {
        wxMessageDialog(this, "Enter path to config file.", "No config specified", wxOK | wxICON_EXCLAMATION).ShowModal();
    }
    else if(!this->HasName())
    {
        wxMessageDialog(this, "Enter your name.", "No name specified", wxOK | wxICON_EXCLAMATION).ShowModal();
    }
    else
    {
        this->EndModal(wxID_OK);
    }
}

void GraphicalUserInterfaceAuthInfo::OnChooseConfig(wxCommandEvent &event)
{
    wxFileDialog configDialog(this, "Open config file", "", "", "Config files (*.cfg)|*.cfg", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if(wxID_CANCEL == configDialog.ShowModal())
    {
        return;
    }
    m_config_path = new wxString(configDialog.GetPath());
    this->ShowConfigPath();
}

void GraphicalUserInterfaceAuthInfo::OnClose(wxCloseEvent &event)
{
    this->EndModal(wxID_CLOSE);
}

void GraphicalUserInterfaceAuthInfo::ShowConfigPath()
{
    m_static_text_config_path->SetLabelText(this->HasConfigPath() ? wxFileName(*m_config_path).GetName() : wxString("(none)"));
}
