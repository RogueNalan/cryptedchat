#include "GraphicalUserInterfaceMain.h"
#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__


BEGIN_EVENT_TABLE(GraphicalUserInterfaceMain, wxDialog)
    EVT_CLOSE(GraphicalUserInterfaceMain::OnClose)
    EVT_TEXT_ENTER(idTextCtrlMessage, GraphicalUserInterfaceMain::OnMessageEnter)
    EVT_BUTTON(idButtonSend, GraphicalUserInterfaceMain::OnSend)
END_EVENT_TABLE()

GraphicalUserInterfaceMain::GraphicalUserInterfaceMain(UserInterfaceListener *listener) :
    wxDialog(NULL, wxID_ANY, "Encrypted Chat"),
    m_listener(listener)
{
    const int kBorderWidth = 5;

    wxBoxSizer* box_sizer_vertical = new wxBoxSizer(wxVERTICAL);
    m_list_box_messages = new wxListBox(this, wxID_ANY, wxDefaultPosition, wxSize(500, 450));
    box_sizer_vertical->Add(m_list_box_messages, 1, (wxALL & ~wxBOTTOM) | wxEXPAND, kBorderWidth);
    wxBoxSizer* box_sizer_horizontal = new wxBoxSizer(wxHORIZONTAL);
    box_sizer_vertical->Add(box_sizer_horizontal, 0, wxEXPAND, kBorderWidth);
    this->SetSizer(box_sizer_vertical);

    m_text_ctrl_message = new wxTextCtrl(this, idTextCtrlMessage, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_PROCESS_ENTER);
    box_sizer_horizontal->Add(m_text_ctrl_message, 1, wxALL & ~wxRIGHT, kBorderWidth);
    m_button_send = new wxButton(this, idButtonSend, "&Send");
    box_sizer_horizontal->Add(m_button_send, 0, wxALIGN_CENTER_VERTICAL | wxALL, kBorderWidth);

    this->SetMinSize(wxSize(200, 150));
    box_sizer_vertical->Fit(this);
}

GraphicalUserInterfaceMain::~GraphicalUserInterfaceMain()
{
}

void GraphicalUserInterfaceMain::AddStringToMessages(wxSharedPtr<wxString> str)
{
    m_list_box_messages->InsertItems(1, &(*str), m_list_box_messages->GetCount());
}

void GraphicalUserInterfaceMain::OnClose(wxCloseEvent &event)
{
    m_listener->OnExit();
}

void GraphicalUserInterfaceMain::OnMessageEnter(wxCommandEvent &event)
{
    this->Send();
}

void GraphicalUserInterfaceMain::OnSend(wxCommandEvent &event)
{
    this->Send();
}

void GraphicalUserInterfaceMain::OpenAuthInfo()
{
    GraphicalUserInterfaceAuthInfo auth_info(this, wxSharedPtr<wxString>(NULL));
    if(wxID_CLOSE == auth_info.ShowModal())
    {
        m_listener->OnExit();
    }
    else
    {
        this->SetLabel(this->GetLabel() + ": " + *auth_info.GetName());
        m_listener->OnAuthInfoChange(auth_info.GetName(), auth_info.GetConfigPath());
    }
}

void GraphicalUserInterfaceMain::Send()
{
    wxSharedPtr<wxString> message(new wxString(m_text_ctrl_message->GetValue()));
    m_listener->OnSend(message);
    m_text_ctrl_message->Clear();
}

void GraphicalUserInterfaceMain::Show()
{
    wxDialog::Show();
    m_text_ctrl_message->SetFocus();
    this->OpenAuthInfo();
}
