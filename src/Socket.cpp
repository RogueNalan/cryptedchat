#include "Socket.h"

BEGIN_EVENT_TABLE(Socket, wxEvtHandler)
    EVT_SOCKET(idSocket, Socket::OnSocketEvent)
END_EVENT_TABLE()

Socket::Socket(wxSharedPtr<wxSocketBase> socket) :
    m_socket(NULL),
    m_listener(NULL)
{
    this->Init(socket);
}

Socket::Socket(wxSharedPtr<wxSocketBase> socket, SocketListener *listener)
{
    this->Init(socket);
    this->SetListener(listener);
}

Socket::~Socket()
{
    //dtor
}

void Socket::Init(wxSharedPtr<wxSocketBase> socket)
{
    m_socket = socket;
    m_socket->SetFlags(wxSOCKET_WAITALL);
    m_socket->SetEventHandler(*this, idSocket);
    m_socket->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
    m_socket->Notify(true);
}

void Socket::OnSocketEvent(wxSocketEvent &event)
{
    wxSocketBase *socket = event.GetSocket();
    uint32_t buffer_size;
    msg_t type;
    switch(event.GetSocketEvent())
    {
    case wxSOCKET_INPUT:
        socket->Read(&buffer_size, sizeof(buffer_size));
        buffer_size = wxUINT32_SWAP_ON_LE(buffer_size);
        socket->Read(&type, sizeof(msg_t));
        switch(type)
        {
        case MsgText:
            {
                wchar_t *buffer = new wchar_t[buffer_size + 1];
                socket->Read(buffer, buffer_size * sizeof(wchar_t));
                buffer[buffer_size] = '\0';
                if(NULL != m_listener)
                {
                    m_listener->OnMessage(wxSharedPtr<wxString>(new wxString(buffer)));
                }
                break;
            }
        case MsgFile:
            {


                //ask
                char *buffer = new char[buffer_size];
                socket->Read(buffer, buffer_size);
                //save file
                break;
            }
        }
        break;
    case wxSOCKET_LOST:
        m_listener->OnSocketLost(this);
        break;
    default:
        wxLogMessage("Unexpected Socket event");
        break;
    }
}

void Socket::Send(wxSharedPtr<wxString> message)
{
    uint32_t buffer_size = message->Length() * sizeof(wchar_t);
    msg_t type = MsgText;
    buffer_size = wxUINT32_SWAP_ON_LE(buffer_size);
    m_socket->Write(&buffer_size, sizeof(buffer_size));
    m_socket->Write(&type, sizeof(msg_t));
    m_socket->Write(message->wc_str(), message->Length());
}

void Socket::SendFile(wxSharedPtr<wxString> filedest)
{
    msg_t type = MsgFile;
    wxFile file(filedest->char_str());
    wxFileOffset offset = file.SeekEnd();
    uint32_t buffer_size = offset;
    char *buffer = new char[buffer_size];
    m_socket->Write(&buffer_size, sizeof(buffer_size));
    m_socket->Write(&type, sizeof(msg_t));
    m_socket->Write(buffer, buffer_size);
}

void Socket::SetListener(SocketListener *listener)
{
    m_listener = listener;
}
