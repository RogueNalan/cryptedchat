#include "SocketServer.h"

BEGIN_EVENT_TABLE(SocketServer, wxEvtHandler)
    EVT_SOCKET(idSocketServer, SocketServer::OnSocketServerEvent)
END_EVENT_TABLE()

SocketServer::SocketServer(SocketServerListener *listener) :
    m_listener(listener)
{
    wxIPV4address address;
    address.Service(3000);
    m_socket_server = wxSharedPtr<wxSocketServer>(new wxSocketServer(address));
    if(m_socket_server->IsOk() && m_socket_server->GetLocal(address))
    {
        wxLogMessage("Server is listening at port %u", address.Service());
    }
    m_socket_server->SetEventHandler(*this, idSocketServer);
    m_socket_server->SetNotify(wxSOCKET_CONNECTION_FLAG);
    m_socket_server->Notify(true);
}

SocketServer::~SocketServer()
{
    //dtor
}

void SocketServer::OnSocketServerEvent(wxSocketEvent &event)
{
    wxSocketBase *socket = m_socket_server->Accept(false);
    switch(event.GetSocketEvent())
    {
    case wxSOCKET_CONNECTION:
        if(NULL != socket)
        {
            m_listener->OnSocketConnected(new Socket(wxSharedPtr<wxSocketBase>(socket)));
        }
        break;
    default:
        wxLogMessage("Unexpected SocketServer event");
        break;
    }
}
