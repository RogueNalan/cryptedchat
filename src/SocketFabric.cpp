#include "SocketFabric.h"

SocketFabric *SocketFabric::m_instance = NULL;

SocketFabric::SocketFabric(wxSharedPtr<Cypher> cypher) :
m_cypher(cypher)
{
}

SocketFabric::~SocketFabric()
{
    //dtor
}

wxSharedPtr<Socket> SocketFabric::CreateSocket(wxSharedPtr<wxIPV4address> address, SocketListener *listener)
{
    wxSocketClient *socket = new wxSocketClient;
    socket->Connect(*address);
    return wxSharedPtr<Socket>(new EncryptedSocket(wxSharedPtr<wxSocketBase>(socket), listener, m_cypher));
}

SocketFabric *SocketFabric::GetInstance(wxSharedPtr<Cypher> cypher)
{
    if(NULL == m_instance){
        m_instance = new SocketFabric(cypher);
    }
    return m_instance;
}
