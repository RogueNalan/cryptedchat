#include "Server.h"

Server *Server::m_instance = NULL;

Server::Server(wxSharedPtr<Config> config) :
    m_sockets(new std::set<Socket*>())
{
    m_socket_server = wxSharedPtr<SocketServer>(new SocketServer(this));
}

Server::~Server()
{
    //dtor
}

Server *Server::GetInstance(wxSharedPtr<Config> config)
{
    if(NULL == m_instance)
    {
        m_instance = new Server(config);
    }
    return m_instance;
}

void Server::OnMessage(wxSharedPtr<wxString> message)
{
    for(std::set<Socket*>::iterator it = m_sockets->begin(); it != m_sockets->end(); ++it)
    {
        (*it)->Send(message);
    }
}

void Server::OnSocketConnected(Socket *socket)
{
    socket->SetListener(this);
    m_sockets->insert(socket);
    wxLogMessage("Socket Connected");
}

void Server::OnSocketLost(Socket *socket)
{
    m_sockets->erase(socket);
    wxLogMessage("Socket lost");
}
