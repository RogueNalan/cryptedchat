#include "Client.h"
#include "Config.h"
#include "Cypher.h"
#include "XORCypher.h"
#include "UserInterfaceBuilder.h"

Client *Client::m_instance = NULL;

Client::Client() :
    UserInterfaceListener(),
    m_name(new wxString("anonymous")),
    m_socket(NULL)
{
    UserInterfaceBuilder user_interface_builder;
    m_user_interface = user_interface_builder.GetUserInterface(this);
    m_user_interface->Show();
}

Client::~Client()
{
    //dtor
}

Client *Client::GetInstance()
{
    if(NULL == m_instance)
    {
        m_instance = new Client();
    }
    return m_instance;
}

void Client::OnAuthInfoChange(wxSharedPtr<wxString> name, wxSharedPtr<wxString> config_path)
{
    Config config(config_path);
    wxSharedPtr<Cypher> cypher (new XORCypher(101));
    m_socket = SocketFabric::GetInstance(cypher)->CreateSocket(config.GetAddress(), this);
    m_name = name;
}

void Client::OnExit()
{
    m_user_interface->Close();
}

void Client::OnMessage(wxSharedPtr<wxString> message)
{
    m_user_interface->ShowMessage(wxSharedPtr<wxString>(new wxString(message->BeforeFirst('\1'))), wxSharedPtr<wxString>(new wxString(message->AfterFirst('\1'))));
}

void Client::OnSend(wxSharedPtr<wxString> message)
{
    this->Send(wxSharedPtr<wxString>(new wxString(*m_name + "\1" + *message)));
}

void Client::OnSocketLost(Socket *socket)
{
}

void Client::Send(wxSharedPtr<wxString> message)
{
    m_socket->Send(message);
}
