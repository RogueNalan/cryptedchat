#include <cstdlib>

#include "Config.h"

Config::Config(wxSharedPtr<wxIPV4address> address) :
    m_address(address),
    m_key1_length(kKey1LengthUsual),
    m_key1(GetKey(m_key1_length)),
    m_key2_length(kKey2LengthUsual),
    m_key2(GetKey(m_key2_length))
{
}

Config::Config(wxSharedPtr<wxString> path) :
    m_address(NULL),
    m_key1(NULL),
    m_key2(NULL)
{
    wxSharedPtr<wxFile> file(new wxFile(*path, wxFile::read));
    this->ReadInt(file); // Version.
    wxIPV4address *address = new wxIPV4address();
    wxSharedPtr<wxString> str(this->ReadString(file));
    address->Hostname(*str);
    address->Service(3000);
    m_address = wxSharedPtr<wxIPV4address>(address);
    std::pair<int, wxSharedPtr<char> > buffer;
    buffer = this->ReadBuffer(file);
    m_key1_length = buffer.first;
    m_key1 = buffer.second;
    buffer = this->ReadBuffer(file);
    m_key2_length = buffer.first;
    m_key2 = buffer.second;
}

Config::~Config()
{
}

wxSharedPtr<wxIPV4address> Config::GetAddress()
{
    return m_address;
}

wxSharedPtr<char> Config::GetKey(int length)
{
    wxSharedPtr<char> buffer(new char[length]);
    for(int i = 0; i < length;)
    {
        int random_value = std::rand();
        for(unsigned int j = 0; i < length && j < sizeof(int); ++j, ++i)
        {
            (&*buffer)[i] = random_value & 0xff;
            random_value >>= 8;
        }
    }
    return buffer;
}

wxSharedPtr<char> Config::GetKey1()
{
    return m_key1;
}

int Config::GetKey1Length()
{
    return m_key1_length;
}

wxSharedPtr<char> Config::GetKey2()
{
    return m_key2;
}

int Config::GetKey2Length()
{
    return m_key2_length;
}

std::pair<int, wxSharedPtr<char> > Config::ReadBuffer(wxSharedPtr<wxFile> file)
{
    int length = this->ReadInt(file);
    wxSharedPtr<char> buffer(new char[length]);
    file->Read(&*buffer, length);
    return std::pair<int, wxSharedPtr<char> >(length, buffer);
}

int Config::ReadInt(wxSharedPtr<wxFile> file)
{
    int number;
    file->Read(&number, sizeof(number));
    return wxINT32_SWAP_ON_LE(number);
}

wxSharedPtr<wxString> Config::ReadString(wxSharedPtr<wxFile> file)
{
    std::pair<int, wxSharedPtr<char> > buffer = this->ReadBuffer(file);
    return wxSharedPtr<wxString>(new wxString((wchar_t*)(&*buffer.second)));
}

void Config::Save(wxSharedPtr<wxString> path)
{
    wxSharedPtr<wxFile> file(new wxFile(*path, wxFile::write));
    this->SaveInt(file, this->kVersion);
    this->SaveString(file, wxSharedPtr<wxString>(new wxString(m_address->IPAddress())));
    this->SaveBuffer(file, m_key1, m_key1_length);
    this->SaveBuffer(file, m_key2, m_key2_length);
}

void Config::SaveBuffer(wxSharedPtr<wxFile> file, wxSharedPtr<char> buffer, int length)
{
    this->SaveInt(file, length);
    file->Write(&*buffer, length);
}

void Config::SaveInt(wxSharedPtr<wxFile> file, const int number)
{
    int n = wxINT32_SWAP_ON_LE(number);
    file->Write(&n, sizeof(n));
}

void Config::SaveString(wxSharedPtr<wxFile> file, wxSharedPtr<wxString> str)
{
    int length = (str->Length() + 1) * sizeof(wchar_t);
    wxSharedPtr<char> buffer(new char[length]);
    memcpy(&*buffer, str->wc_str(), length);
    this->SaveBuffer(file, buffer, length);
}
