#include "XORCypher.h"

XORCypher::XORCypher(char key)
    : key (key)
{
    //ctor
}

XORCypher::~XORCypher()
{
    //dtor
}

wxSharedPtr<char> XORCypher::Encrypt(wxSharedPtr<char> message, int length)
{
    return Convert(message, length);
}

wxSharedPtr<char> XORCypher::Decrypt(wxSharedPtr<char> buffer, int length)
{
    return Convert(buffer, length);
}

wxSharedPtr<char> XORCypher::Convert(wxSharedPtr<char> message, int length)
{
    wxSharedPtr<char> res(new char[length]);
    char* dst = res.get();
    char* src = message.get();

    for (int i = 0; i < length; ++i)
        dst[i] = src[i] ^ key;
    return res;
}
