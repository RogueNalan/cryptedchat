#include "UserInterfaceBuilder.h"
#include "GraphicalUserInterface.h"

UserInterfaceBuilder::UserInterfaceBuilder()
{
    //ctor
}

UserInterfaceBuilder::~UserInterfaceBuilder()
{
    //dtor
}

wxSharedPtr<UserInterface> UserInterfaceBuilder::GetUserInterface(UserInterfaceListener *user_interface_listener)
{
    UserInterface *user_interface = new GraphicalUserInterface(user_interface_listener);
    return wxSharedPtr<UserInterface>(user_interface);
}
