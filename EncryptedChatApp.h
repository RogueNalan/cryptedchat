#ifndef ENCRYPTEDCHATAPP_H
#define ENCRYPTEDCHATAPP_H

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/cmdline.h>

#include "Client.h"
#include "Config.h"
#include "Server.h"

class EncryptedChatApp : public wxApp
{
public:
    virtual bool OnCmdLineParsed(wxCmdLineParser &parser);
    virtual void OnInitCmdLine(wxCmdLineParser &parser);
};

static const wxCmdLineEntryDesc kCmdLineDesc [] =
{
    { wxCMD_LINE_SWITCH, "h", "help", "displays help", wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
    { wxCMD_LINE_OPTION, "s", "server", "starts program in server mode" },
    { wxCMD_LINE_OPTION, "c", "config", "generate config", wxCMD_LINE_VAL_STRING },
    { wxCMD_LINE_OPTION, "p", "path", "path to config", wxCMD_LINE_VAL_STRING },
    { wxCMD_LINE_NONE }
};

#endif // ENCRYPTEDCHATAPP_H
